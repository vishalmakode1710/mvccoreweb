﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductWebApplication.MVC.Models
{
    public class Product
    {
        #region Properties
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public bool IsAvailable { get; set; }
        public double Price { get; set; } = 1500.00;
        public string? Location { get; set; }
        public string? ds { get; set; }
        #endregion
    }
}
